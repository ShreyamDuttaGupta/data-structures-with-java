/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ShreyamDuttaGupta
 */

import java.util.HashMap;

public class isUnique {
    
    /*
    Finding unique characters using Nested for loop. 
    Complexity is O(n2). 
    */
    
    
    /*
    public static boolean isUnique(String s){
        char word;
        boolean answer = true;
        for(int i=0; i<s.length(); i++){
            word = s.charAt(i);
            for(int j = 0; j<s.length(); j++){
                if(j != i){
                    if(s.charAt(j) == word && s.charAt(j) != ' '){
                        answer = false;
                    }
                }
            }
        }
    return answer;
    }   */
        
   /*
    Finding unique characters using HashMap Technique. 
    Complexity is O(n). 
    */
    
    
    private static boolean isUnique(String s){ 
        HashMap<Character, Integer> newMap = new HashMap<>();
        for(char ch : s.toCharArray()){
            if(!newMap.containsKey(ch))
                newMap.put(ch,1);
             else
                return false;
        }
       return true;
    }
      
    public static void main(String[] args){  
        
        boolean x = isUnique("qwerty");
        System.out.println(x);
        if(x)
             System.out.println("All unique");
        else
             System.out.println("Not all unique");
    } 
}
