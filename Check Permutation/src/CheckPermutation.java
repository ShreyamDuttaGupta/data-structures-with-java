/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ShreyamDuttaGupta
 */
public class CheckPermutation {
    
    public static boolean checkStrings(String str1, String str2){
        
        if(str1.length() != str2.length()){
            return false;
        }
        
        boolean answer = true;
        char [] newStr1 = sortString(str1);
        char [] newStr2 = sortString(str2);
        
        for(int i = 0; i<newStr1.length; i++){
            if(newStr1[i] != newStr2[i]){
                answer = false;
                break;
            }
        }
        return answer;
    }
    
    public static char [] sortString(String s){
        char [] newStr = new char[s.length()];
        for(int i = 0; i<s.length(); i++){
            newStr[i] = s.charAt(i);
        }
        java.util.Arrays.sort(newStr);
        for(int i = 0; i<newStr.length; i++){
            System.out.println(newStr[i]);
        }
        return newStr;
    }

    
    public static void main(String[] args){  
        
        //String palin = checkWords("b yh", "hy b");
        boolean answer = checkStrings("god sh ", "dog s h");
        System.out.println(answer);
    } 
    
}
